import random

"""
merge two sorted lists
"""


def merge(a, b):
    i = 0
    j = 0
    res = []

    while i < len(a) and j < len(b):
        if a[i] < b[j]:
            res.append(a[i])
            i += 1
        elif a[i] > b[j]:
            res.append(b[j])
            j += 1
        else:
            res.append(a[i])
            res.append(b[j])
            i += 1
            j += 1

    if len(a) == i:
        res.extend(b[j:])
    elif len(b) == j:
        res.extend(a[i:])

    return res


if __name__ == '__main__':
    max_size = 10
    a = sorted([random.randint(0, max_size) for _ in range(random.randint(1, max_size))])
    b = sorted([random.randint(0, max_size) for _ in range(random.randint(1, max_size))])
    print(a)
    print(b)
    print(merge(a, b))
