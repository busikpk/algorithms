"""
Write queue only on two stacks
"""


class Stack(object):

    def __init__(self):
        self.__arr = []

    def is_empty(self):
        return len(self.__arr) == 0

    def push(self, obj):
        self.__arr.insert(0, obj)

    def pop(self):
        if self.is_empty():
            return None
        return self.__arr.pop(0)

    def __str__(self):
        return '{}'.format(self.__arr)


class Queue(object):

    def __init__(self):
        self.__input = Stack()
        self.__output = Stack()

    def add(self, obj):
        self.__input.push(obj)

    def pop(self):
        if self.__output.is_empty():
            while not self.__input.is_empty():
                self.__output.push(self.__input.pop())

        return self.__output.pop() if not self.__output.is_empty() else None

    def __str__(self):
        return '{} {}'.format(self.__output, self.__input)

if __name__ == '__main__':
    queue = Queue()

    [queue.add(i) for i in range(10)]

    print(queue.pop())
    print(queue.pop())
    print(queue.pop())

    [queue.add(i) for i in range(10, 15)]

    print(queue.pop())