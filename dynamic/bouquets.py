import time

"""
how match combinations of goods with prices p1, p2, p3 your can buy with summ $
"""


def bouquets(p1, p2, p3, summ):
    a, b, c = sorted([p1, p2, p3])
    count = 0

    for i in range(summ // a + 1):
        for j in range(summ // b + 1):
            s = i * a + j * b
            if s > summ:
                break
            tfc = (summ - s) // c
            count += tfc // 2 if tfc % 2 == 0 and (i + j) % 2 == 0 else tfc // 2 + 1

    return count


if __name__ == '__main__':
    start = time.time()
    print(bouquets(1, 1, 1, 5))
    print(bouquets(2, 3, 4, 10))
    print(bouquets(2, 3, 4, 100))
    print(bouquets(200, 300, 400, 10000))
    print(bouquets(200, 300, 400, 100000))
    print(bouquets(22, 44, 150, 20000))
    print(time.time() - start)
