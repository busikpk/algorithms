"""
Print binary tree level by level
"""


class Node(object):

    def __init__(self, id):
        self.id = id
        self.left = None
        self.right = None


def group_by_levels(root, level_id, table):
    if root is None:
        return table
    if level_id in table:
        table[level_id].append(root.id)
    else:
        table[level_id] = [root.id]
    group_by_levels(root.left, level_id + 1, table)
    group_by_levels(root.right, level_id + 1, table)
    return table

if __name__ == '__main__':
    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)
    root.left.left = Node(4)
    root.left.right = Node(5)

    table = group_by_levels(root, 0, {})
    for k in sorted(table.keys()):
        print(table[k])

