import os

"""
write function which takes the name of a directory and prints out the paths files within that
directory as well as any files contained in contained directories.
"""


def printer(path):
    try:
        for p in os.listdir(path):
            p0 = path+'/'+p
            if os.path.isdir(p0):
                printer(p0)
            else:
                print(p0)
    except PermissionError:
        print('Permission Denied for {}'.format(path))


if __name__ == '__main__':
    printer('C://Users')
