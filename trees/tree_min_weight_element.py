"""
find tree node with min weight
"""


class Node(object):
    def __init__(self, node_id, weight, neighbors=None):
        if neighbors is None:
            neighbors = []
        self.neighbors = neighbors
        self.id = node_id
        self.weight = weight


def min_node(root):
    if not root.neighbors:
        return root.weight
    return min([min_node(n) for n in root.neighbors] + [root.weight])


if __name__ == '__main__':
    #              1(5)
    # 2(6)    3(7)      4(8)      5(9)
    # 6(3)    7(0)   8(4)  9(2)   10(-1)

    n10 = Node(10, -1)
    n9 = Node(9, 2)
    n8 = Node(8, 4)
    n7 = Node(7, 0)
    n6 = Node(6, 3)
    n5 = Node(5, 9, [n10])
    n4 = Node(4, 8, [n8, n9])
    n3 = Node(3, 7, [n7])
    n2 = Node(2, 6, [n6])
    n1 = Node(1, 5, [n2, n3, n4, n5])

    print(min_node(n1))
